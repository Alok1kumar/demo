package com.hcl.facility.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.facility.entity.Facility;
import com.hcl.facility.exception.CustomException;
import com.hcl.facility.repository.FacilityRepository;
@Service
public class FaclityServiceImpl  implements FaclityService{
@Autowired
FacilityRepository facilityRepository;
	@Override
	public String saveFacility(Facility facility) {
		// TODO Auto-generated method stub
		 Facility save = facilityRepository.save(facility);
		 return "success";
	}

	@Override
	public List<Facility> getFacilities() {
		// TODO Auto-generated method stub
		return facilityRepository.findAll();
	}

	@Override
	public Optional<Facility> getFacilityById(Long faclityid) {
		// TODO Auto-generated method stub
		return facilityRepository.findById(faclityid);
	}

	@Override
	public String deleteFacilityId(Long faclityid) {
		// TODO Auto-generated method stub
		Optional<Facility> findById = facilityRepository.findById(faclityid);
		if(!findById.isPresent()) {
			throw new CustomException("faclity id is not present");
		}
		facilityRepository.deleteById(faclityid);
		return "succeess";
	}

}
