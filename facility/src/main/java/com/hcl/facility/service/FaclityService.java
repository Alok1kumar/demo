package com.hcl.facility.service;

import java.util.List;
import java.util.Optional;


import com.hcl.facility.entity.Facility;

public interface FaclityService {
	public String saveFacility(Facility facility);
    public List<Facility> getFacilities();
    public Optional<Facility> getFacilityById(Long faclityid);
    public String deleteFacilityId(Long faclityid);

}
