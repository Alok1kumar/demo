package com.hcl.facility.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
@RestController
public class FaclityControllerRest {
	@Autowired
	 RestTemplate restTemplate;

	    @GetMapping("/swipedetails")
	    public ResponseEntity< List<Object>> getSwipeDetails() {
	    	String uri="http://localhost:9090/swipedetails";
	    	Object[] objects=restTemplate.getForObject(uri, Object[].class);
			return  new ResponseEntity<List<Object>>( Arrays.asList(objects),HttpStatus.OK);
		
	        
	    }
	    

	 

	    @GetMapping(value = "/showdetails/{employeesapId}")
	    public ResponseEntity<Object> getSwipedetailsWithEmployeeid(@PathVariable("employeesapId") int employeesapId) {
	    	
	    	String url="http://localhost:9090/showdetails/{employeesapId}";
	    	Object forObject = restTemplate.getForObject(url, Object.class,employeesapId);
			return new ResponseEntity<Object>(forObject,HttpStatus.ACCEPTED);
	    	
	    }
	      
	        
	    }
	




