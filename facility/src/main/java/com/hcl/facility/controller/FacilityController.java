package com.hcl.facility.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


import com.hcl.facility.entity.Facility;
import com.hcl.facility.service.FaclityService;


@RestController
public class FacilityController {
	
	@Autowired
	FaclityService faclityService;
	 @RequestMapping(value = "/faclity", method = RequestMethod.POST)
	    public ResponseEntity< String> saveFacility(@RequestBody Facility faclity) {
	        return new ResponseEntity<String>(faclityService.saveFacility(faclity),HttpStatus.OK);
	    }

	 

	    @GetMapping("/faclitye")
	    public ResponseEntity<List<Facility>> getFacility() {
	        return new ResponseEntity<List<Facility>>(faclityService.getFacilities(),HttpStatus.ACCEPTED);
	    }

	 

	    @GetMapping(value = "/faclity/{facid}")
	    public ResponseEntity<Optional<Facility>> getFacility(@PathVariable Long facid ) {
	     Optional<Facility> facilityById = faclityService.getFacilityById( facid );
			return new ResponseEntity<Optional<Facility>>(facilityById,HttpStatus.CREATED);
	        
	    }
	    @DeleteMapping(value = "/deletefaclity/{facid}")
	   ResponseEntity<String> deleteFacility(@PathVariable Long facid){
		 return new ResponseEntity<String>( faclityService.deleteFacilityId(facid),HttpStatus.OK);
	   }
	
	
	
}
